Juego pygame.


## nivel 1: vista superior, modificadores y generadores de enemigos.
https://youtu.be/InBeQisWl5M

Implementar juego con las siguientes caracteristicas:

1) Dos (2) tipos Generadores de enemigos (uno indestructible, otro vulnerable), cada uno debe generar enemigos distintos tanto en comportamiento como en apariencia, diseminados a los largo del ambiente (al menos 10 generadores en el ambiente, todos activos). 

2) Fondo desplazante (imagen mas grande que la ventana 3 veces alto y 4 veces ancho de la panatalla) 

3) Objetos y enemigos diseminados en espacio no visible (fuera de la pantalla y fijo respecto al fondo) 

4) Modo 1 jugador, debe poseer nivel de vida, debe ser visible la inforamcion del jugador  en la pantalla siempre. 

5) Extras: animaciones de sprites (jugador y enemigos), musica y efectos de sonido, fin de juego, jugabilidad, 3 modificadores de juego correctamente implementados, deben generarse al azar. 

6) Animacion y comportamiento de enemigos 


## nivel 2: plataforma, enemigo estatico y dinamico
https://youtu.be/9qYffuU4rsA

Documento: 
Tabla 3, Plantilla del Documento de Diseño (archvio disvideojuegos.pdf) 

Agregar un nivel plataforma:
al menos 3 veces la pantalla de ancho, 2 enemigos distintos (1 tipo estatico, 1 tipo dinamico) repartidos a lo largo del nivel, 1 Jefe. 
