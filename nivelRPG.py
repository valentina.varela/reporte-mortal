import pygame
from pytmx.util_pygame import load_pygame
import random
import nivelPLataforma


verde = [0,255,0]
rojo = [255,0,0]
azul = [0,0,255]
blanco = [255,255,255]
negro = [0,0,0]
turquesa = [83,166,154]
lila = [203,139,218]
naranja = [225,170,159]

ancho = 800
alto = 800


class modificadorGeneradores(pygame.sprite.Sprite):
    def __init__(self,m,posicion):
        pygame.sprite.Sprite.__init__(self)
        self.image = m
        self.rect = self.image.get_rect()
        self.rect.x = 0
        self.rect.y = 0
        self.x = posicion[0]
        self.y = posicion[1]
        self.entra = 0
        self.destruido = 0
        
    def update(self,posicionVentana,jugador):
        self.rect.x = posicionVentana[0] + self. x
        self.rect.y = posicionVentana[1] + self. y


class modificadorBotiquines(pygame.sprite.Sprite):
    def __init__(self,m,posicion):
        pygame.sprite.Sprite.__init__(self)
        self.m = m
        self.col = 0
        self.fil = 0 #animacion de abrir y cerrar cofre
        self.image = self.m[self.fil][self.col]
        self.rect = self.image.get_rect()
        self.rect.x = 0
        self.rect.y = 0
        self.x = posicion[0]
        self.y = posicion[1]

       
    def update(self,posicionVentana,jugador):
        self.image = self.m[self.fil][self.col]
        self.rect.x = posicionVentana[0] + self. x
        self.rect.y = posicionVentana[1] + self. y

 

class modificadorInformes(pygame.sprite.Sprite):
    def __init__(self,m,posicion):
        pygame.sprite.Sprite.__init__(self)
        self.m = m
        self.col = 3
        self.fil = 0 #animacion de abrir y cerrar cofre
        self.image = self.m[self.fil][self.col]
        self.rect = self.image.get_rect()
        self.rect.x = 0
        self.rect.y = 0
        self.x = posicion[0]
        self.y = posicion[1]
        self.abierto = 0
       
    def update(self,posicionVentana,jugador):
        self.image = self.m[self.fil][self.col]
        self.rect.x = posicionVentana[0] + self. x
        self.rect.y = posicionVentana[1] + self. y
        chest = pygame.Rect(self.rect.x,self.rect.y-16,30,30)
        if (jugador.colliderect(chest)): 
            self.abierto = 1
            self.fil = 3




class generadorInfo(pygame.sprite.Sprite):
    def __init__(self,m):
        pygame.sprite.Sprite.__init__(self)
        self.m = m
        self.col = 0
        self.fil = 4 #esta es la que cambia la cantidad de generadores
        self.image = self.m[self.fil][self.col]
        self.rect = self.image.get_rect()
        self.rect.x = 450
        self.rect.y = 730
        self.generadores = 0
        
    def update(self):
        self.image = self.m[self.fil][self.col]
        if self.generadores == 0:
            self.fil = 0
        if self.generadores == 1:
            self.fil = 1
        if self.generadores == 2:
            self.fil = 2
        if self.generadores == 3:
            self.fil = 3
        if self.generadores == 4:
            self.fil = 4




class Informes(pygame.sprite.Sprite):
    def __init__(self,m):
        pygame.sprite.Sprite.__init__(self)
        self.m = m
        self.col = 0
        self.fil = 0 #esta es la que cambia la cantidad de informes
        self.image = self.m[self.fil][self.col]
        self.rect = self.image.get_rect()
        self.rect.x = 530
        self.rect.y = 680
        self.informes = 0
        
    def update(self):
        self.image = self.m[self.fil][self.col]
        if self.informes == 0:
            self.fil = 0
        if self.informes == 1:
            self.fil = 1
        if self.informes == 2:
            self.fil = 2
        if self.informes == 3:
            self.fil = 3




class Informacion(pygame.sprite.Sprite):
    def __init__(self,m):
        pygame.sprite.Sprite.__init__(self)
        self.m = m
        self.col = 0
        self.fil = 0 #esta es la que cambia la imagen de la vida
        self.image = self.m[self.fil][self.col]
        self.rect = self.image.get_rect()
        self.rect.x = 10
        self.rect.y = 10
        self.salud = 5
        
    def update(self):
        self.image = self.m[self.fil][self.col]
        if self.salud == 5:
            self.fil = 0
        if self.salud == 4:
            self.fil = 1
        if self.salud == 3:
            self.fil = 2
        if self.salud == 2:
            self.fil = 3
        if self.salud == 1:
            self.fil = 4


class Bala(pygame.sprite.Sprite):

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface([10, 10])
        self.image.fill(verde)
        self.rect = self.image.get_rect()
        self.disparo = 0


    def update(self,direccion):
        if direccion == 10:
            self.rect.y += 15
        if direccion == 8:
            self.rect.y -= 15
        if direccion == 11:
            self.rect.x += 15
        if direccion == 9:
            self.rect.x -= 15



class Enemigo(pygame.sprite.Sprite):
    
    def __init__(self,m,posicion,lim1,lim2):
        pygame.sprite.Sprite.__init__(self)
        self.m = m
        self.col = 0
        self.fil = 11
        self.image = self.m[self.fil][self.col]
        self.rect = self.image.get_rect()
        self.rect.x = 0
        self.rect.y = 0
        self.x = posicion[0]
        self.y = posicion[1]
        self.izquierda = lim1
        self.derecha = lim2
        self.velx = 2
        
    def update(self,posicionVentana):
        self.image = self.m[self.fil][self.col]
        
        self.rect.x = posicionVentana[0] + self. x 
        self.rect.y = posicionVentana[1] + self. y
        self.x += self.velx

        
        
        if (self.fil <= 3) and (self.fil >=0):
            if self.col < 6:
                self.col += 1
            else:
                self.col = 0
        if (self.fil <= 7) and (self.fil >=4):
            if self.col < 7:
                self.col += 1
            else:
                self.col = 0
        if (self.fil <= 11) and (self.fil >=8):
            if self.col < 8:
                self.col += 1
            else:
                self.col = 0
        if ((self.fil <= 15) and (self.fil >=12)) or self.fil == 20:
            if self.col < 5:
                self.col += 1
            else:
                self.col = 0
        if (self.fil <= 19) and (self.fil >=16):
            if self.col < 12:
                self.col += 1
            else:
                self.col = 0
                
        if self.rect.x < posicionVentana[0] +self.izquierda:
            self.velx = 2
            self.fil = 11

        # Limita el margen derecho
        if self.rect.x > posicionVentana[0]+ self.derecha:
            self.velx = -2
            self.fil = 9
            
            

class EnemigoVertical(pygame.sprite.Sprite):
    
    def __init__(self,m,posicion,lim1,lim2):
        pygame.sprite.Sprite.__init__(self)
        self.m = m
        self.col = 0
        self.fil = 10
        self.image = self.m[self.fil][self.col]
        self.rect = self.image.get_rect()
        self.rect.x = 0
        self.rect.y = 0
        self.x = posicion[0]
        self.y = posicion[1]
        self.arriba = lim1
        self.abajo = lim2
        self.vely = 5
        
    def update(self,posicionVentana):
        self.image = self.m[self.fil][self.col]
        
        self.rect.x = posicionVentana[0] + self. x 
        self.rect.y = posicionVentana[1] + self. y
        self.y += self.vely

        
        
        if (self.fil <= 3) and (self.fil >=0):
            if self.col < 6:
                self.col += 1
            else:
                self.col = 0
        if (self.fil <= 7) and (self.fil >=4):
            if self.col < 7:
                self.col += 1
            else:
                self.col = 0
        if (self.fil <= 11) and (self.fil >=8):
            if self.col < 8:
                self.col += 1
            else:
                self.col = 0
        if ((self.fil <= 15) and (self.fil >=12)) or self.fil == 20:
            if self.col < 5:
                self.col += 1
            else:
                self.col = 0
        if (self.fil <= 19) and (self.fil >=16):
            if self.col < 12:
                self.col += 1
            else:
                self.col = 0
                
        if self.rect.y < posicionVentana[1] +self.arriba:
            self.vely = 5
            self.fil = 10

        # Limita el margen derecho
        if self.rect.y > posicionVentana[1]+ self.abajo:
            self.vely = -5
            self.fil = 8


  

class Jugador(pygame.sprite.Sprite):
    
    def __init__(self,m):
        pygame.sprite.Sprite.__init__(self)
        self.m = m
        self.col = 0
        self.fil = 10
        self.image = self.m[self.fil][self.col]
        self.rect = self.image.get_rect()
        self.rect.x = 30
        self.rect.y = 650
        self.velx = 0
        self.vely = 0
        self.entra = 0
        self.entra2 = 0
        self.posicion = 0
        self.informes = 0
        self.salud = 5  #inicia con 5 de vida
        self.generadores = 0#generadores destruidos
        self.disparo = 0



       
    def update(self):
        if self.entra == 0:#animacion en movimiento
            if self.velx != self.vely:
                self.image = self.m[self.fil][self.col]
                if (self.fil <= 3) and (self.fil >=0):
                    if self.col < 6:
                        self.col += 1
                    else:
                        self.col = 0
                if (self.fil <= 7) and (self.fil >=4):
                    if self.col < 7:
                        self.col += 1
                    else:
                        self.col = 0
                if (self.fil <= 11) and (self.fil >=8):
                    if self.col < 8:
                        self.col += 1
                    else:
                        self.col = 0
                if ((self.fil <= 15) and (self.fil >=12)) or self.fil == 20:
                    if self.col < 5:
                        self.col += 1
                    else:
                        self.col = 0
                if (self.fil <= 19) and (self.fil >=16):
                    if self.col < 12:
                        self.col += 1
                    else:
                        self.col = 0
                        
            self.rect.x += self.velx
            self.rect.y += self.vely
            
            if self.rect.right > ancho:
                    self.rect.right = ancho

            if self.rect.left < 0:
                self.rect.left = 0
                            
            if self.rect.bottom > alto:
                self.rect.bottom = alto

            if self.rect.top < 0:
                    self.rect.top = 0
                    
        elif self.entra == 1:#animacion sin movimiento
            self.image = self.m[self.fil][self.col]
            if (self.fil <= 3) and (self.fil >=0):
                if self.col < 6:
                    self.col += 1
                else:
                    self.col = 0
            if (self.fil <= 7) and (self.fil >=4):
                if self.col < 7:
                    self.col += 1
                else:
                    self.col = 0
            if (self.fil <= 11) and (self.fil >=8):
                if self.col < 8:
                    self.col += 1
                else:
                    self.col = 0
            if ((self.fil <= 15) and (self.fil >=12)) or self.fil == 20:
                if self.col < 5:
                     self.col += 1
                else:
                    self.col = 0
            if (self.fil <= 19) and (self.fil >=16):
                if self.col < 12:
                    self.col += 1
                else:
                    self.col = 0



        

def recortar(i_ancho,i_alto,m):#funcion de recorte de sprites
    info=m.get_rect()
    f_ancho=info[2]
    f_alto=info[3]
    mls=list()
    ls=list()
    filas=i_alto
    col=i_ancho
    corte_ancho=int(f_ancho/col)
    corte_alto=int(f_alto/filas)
    for i in range(filas):
        ls=[]
        for j in range(col):
            cuadrado =m.subsurface((corte_ancho*j),(corte_alto*i),corte_ancho,corte_alto)
            ls.append(cuadrado)
        mls.append(ls)

    return mls



def checkbounds(posicionVentana,jugador,tiles):#funcion para verificar si el jugador colisiona con el mapa
    check = False
    X = (posicionVentana[0]*-1)+jugador.x
    Y = (posicionVentana[1]*-1)+jugador.y
    rectangulo = pygame.Rect(X+8,Y,40, 64)
    if (rectangulo.collidelistall(tiles)):
        check = True
    return check



def nivel1(screen):
    
    Ganar = 0
    
    #posicion inicial de la camara
    f_posx = 0
    f_posy = -1008
    anterior = 10#imagen inicial del jugador
    
    
    sonido = pygame.mixer.Sound("historia.wav")
    pygame.mixer.Sound.play(sonido, 1)
    inicio = pygame.image.load("historia.png")
    screen.fill(negro)
    screen.blit(inicio,(0,0))
    pygame.display.flip()
    pygame.time.wait(15000)#20 segundos para leer la historia
    inicio = pygame.image.load("instrucciones.png")
    screen.fill(negro)
    screen.blit(inicio,(0,0))
    pygame.display.flip()
    pygame.time.wait(3000)
    
 
    
    fondo = []
    fondo = [pygame.image.load("mapa.png"),pygame.image.load("ganar.png"),pygame.image.load("perder.png")]#se carga una imagen del mapa
    seleccion = 0

    #se leen las capas del mapa
    tiled_map = load_pygame('mapa.tmx', pixelalpha=True)
    tilewidth = tiled_map.tilewidth
    tileheight = tiled_map.tileheight
    collision = tiled_map.get_layer_by_name('collision')#colisiones como paredes y objetos
    
    #se guarda en un arreglo la informacion de los datos colisionables
    tiles = []
    for x, y, tile in collision.tiles():
            if (tile):
                tiles.append(pygame.Rect([(x*tilewidth), (y*tileheight), tilewidth, tileheight]));
        
    #CREACION DE LOS GRUPOS: Informacion
    Info =   pygame.sprite.Group()  
    imagen=pygame.image.load('vida.png')
    imagen_redimensionada = pygame.transform.scale(imagen, (230, 230))
    m = recortar(1, 5, imagen_redimensionada) #[fila][columna] 
    I = Informacion(m)
    Info.add(I)
    
    #CREACION DE LOS GRUPOS: Informes
    Informes1 =   pygame.sprite.Group()  
    imagen=pygame.image.load('informes.png')
    imagen_redimensionada = pygame.transform.scale(imagen, (250, 250))
    m = recortar(1, 4, imagen_redimensionada) #[fila][columna]   
    informes1 = Informes(m)
    Informes1.add(informes1)
    
    #CREACION DE LOS GRUPOS: Informacion generadores
    GeneradoresInfo =   pygame.sprite.Group()  
    imagen=pygame.image.load('generadores.png')
    imagen_redimensionada = pygame.transform.scale(imagen, (320, 320))
    m = recortar(1, 5, imagen_redimensionada) #[fila][columna]
    generadoresInformacion = generadorInfo(m)
    GeneradoresInfo.add(generadoresInformacion)
    
    #CREACION DE LOS GRUPOS: Jugador
    jugadores=pygame.sprite.Group()
    img=pygame.image.load('arco.png')
    m = recortar(13, 21, img) #[fila][columna]
    j1 = Jugador(m)
    jugadores.add(j1)
    
    #CREACION DE LOS GRUPOS: Zombies
    zombies=pygame.sprite.Group()
    img=pygame.image.load('zombie.png')
    m = recortar(13, 21, img) #[fila][columna]
    #movimiento horizontal
    z = Enemigo(m,[300,300],120,350)
    zombies.add(z)
    z = Enemigo (m,[698,45],498,1444)
    zombies.add(z)
    z = Enemigo (m,[600,546],498,982)
    zombies.add(z)
    z = Enemigo (m,[900,752],745,1240)
    zombies.add(z)
    z = Enemigo (m,[1000,1458],951,1212)
    zombies.add(z)
    z = Enemigo (m,[1000,1897],279,2121)
    zombies.add(z)
    z = Enemigo (m,[400,1446],188,618)
    zombies.add(z)
    z = Enemigo (m,[1500,412],1460,1712)
    zombies.add(z)
    z = Enemigo (m,[1500,412],1460,1712)
    zombies.add(z)
    z = Enemigo (m,[200,2132],293,1889)
    zombies.add(z)
    z = Enemigo (m,[1500,2250],1145,2243)
    zombies.add(z)
    z = Enemigo (m,[2100,1388],1937,2266)
    zombies.add(z)
    z = Enemigo (m,[2000,634],1888,2294)
    zombies.add(z)
    z = Enemigo (m,[2050,1730],1830,2263)
    zombies.add(z)
    z = Enemigo (m,[300,600],214,428)
    zombies.add(z)
    z = Enemigo (m,[730,1287],526,1018)
    zombies.add(z)
    z = Enemigo (m,[900,973],883,1158)
    zombies.add(z)
    z = Enemigo (m,[320,1019],200,400)
    zombies.add(z)
    z = Enemigo (m,[500,1707],319,592)
    zombies.add(z)
    z = Enemigo (m,[100,1713],942,1163)
    zombies.add(z)
    z = Enemigo (m,[1200,1291],739,1323)
    zombies.add(z)
    z = Enemigo (m,[1200,230],906,1260)
    zombies.add(z)
    z = Enemigo (m,[810,311],728,982)
    zombies.add(z)
    z = Enemigo (m,[810,2038],171,952)
    zombies.add(z)
    z = Enemigo (m,[1415,2171],1103,1831)
    zombies.add(z)
    #movimiento vertical
    img=pygame.image.load('jefe.png')
    m = recortar(13, 21, img) #[fila][columna]
    z = EnemigoVertical (m,[64,1400],914,1560)
    zombies.add(z)
    z = EnemigoVertical (m,[507,300],141,978)
    zombies.add(z)
    z = EnemigoVertical (m,[507,1300],1006,1440)
    zombies.add(z)
    z = EnemigoVertical (m,[364,1300],1039,1254)
    zombies.add(z)
    z = EnemigoVertical (m,[321,750],703,865)
    zombies.add(z)
    z = EnemigoVertical (m,[371,1500],1445,1745)
    zombies.add(z)
    z = EnemigoVertical (m,[740,900],857,1087)
    zombies.add(z)
    z = EnemigoVertical (m,[577,1400],1338,1457)
    zombies.add(z)
    z = EnemigoVertical (m,[937,1600],1591,1735)
    zombies.add(z)
    z = EnemigoVertical (m,[863,500],335,710)
    zombies.add(z)
    z = EnemigoVertical (m,[1426,1100],345,1238)
    zombies.add(z)
    z = EnemigoVertical (m,[1708,1300],1206,1530)
    zombies.add(z)
    z = EnemigoVertical (m,[2220,1100],1047,1357)
    zombies.add(z)
    z = EnemigoVertical (m,[1907,1200],1091,1349)
    zombies.add(z)
    z = EnemigoVertical (m,[1907,1500],317,1914)
    zombies.add(z)
    z = EnemigoVertical (m,[253,1900],1854,2303)
    zombies.add(z)
    z = EnemigoVertical (m,[864,2000],1854,2303)
    zombies.add(z)
    z = EnemigoVertical (m,[1463,2300],1854,2303)
    zombies.add(z)
    z = EnemigoVertical (m,[2207,1800],1797,2257)
    zombies.add(z)
    
    
    #CREACION DE LOS MODIFICADORES: Cofres
    cofres=pygame.sprite.Group()
    img=pygame.image.load('cofres.png')
    m = recortar(12, 8, img) #[fila][columna]
    c1 = modificadorInformes(m,[1235,161])
    cofres.add(c1)
    c2 = modificadorInformes(m,[10,800])
    cofres.add(c2)
    c3 = modificadorInformes(m,[2213,131])
    cofres.add(c3)
    
    #CREACION DE LOS MODIFICADORES: Botiquines
    botiquines=pygame.sprite.Group()
    img=pygame.image.load('botiquin.png')
    img = pygame.transform.scale(img, (40, 40))
    m = recortar(1, 1, img) #[fila][columna]
    b1 = modificadorBotiquines(m,[1161,1024])
    botiquines.add(b1)
    b2 = modificadorBotiquines(m,[405,1724])
    botiquines.add(b2)
    b3 = modificadorBotiquines(m,[2257,108])
    botiquines.add(b3)
    b4 = modificadorBotiquines(m,[628,1900])
    botiquines.add(b4)
    
    #CREACION DE LOS MODIFICADORES: Generadores
    gen=pygame.sprite.Group()
    img=pygame.image.load('generador.png')
    #img = pygame.transform.scale(img, (40, 40))
    g1 = modificadorGeneradores(img,[10,500])
    gen.add(g1)
    g2 = modificadorGeneradores(img,[736,1092])
    gen.add(g2)
    g3 = modificadorGeneradores(img,[77,2028])
    gen.add(g3)
    g4 = modificadorGeneradores(img,[741,200])
    gen.add(g4)
    
    
    #Flechas del jugador
    flechas = pygame.sprite.Group()
    
    
    reloj = pygame.time.Clock()
    fin = False
    
    
    sonido.stop()
    sonido = pygame.mixer.Sound("ciclo.mp3")
    pygame.mixer.Sound.play(sonido, -1)
    #bucle del juego
    while not fin:
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                fin = True

            if event.type == pygame.KEYDOWN:
                j1.entra = 0#comprobar si esta disparando
                j1.entra2 = 0#comprobar si esta parado
                j1.velx = 0
                j1.vely = 0
                if event.key == pygame.K_d:
                    j1.fil = 11
                    anterior = j1.fil
                    j1.rect.x += 5
                    if checkbounds([f_posx,f_posy],j1.rect,tiles) == False:
                        j1.velx = 5
                    else: 
                        j1.rect.x += -15
                        
                if event.key == pygame.K_a:
                    j1.fil = 9
                    anterior = j1.fil
                    j1.rect.x += -5
                    if checkbounds([f_posx,f_posy],j1.rect,tiles) == False:
                        j1.velx = -5
                    else:
                        j1.rect.x += 15

                if event.key == pygame.K_w:
                    j1.fil = 8
                    anterior = j1.fil
                    if checkbounds([f_posx,f_posy],j1.rect,tiles) == False:
                        j1.vely = -5
                    else: 
                        j1.rect.top += 15
                        
                if event.key == pygame.K_s:
                    j1.fil = 10
                    anterior = j1.fil
                    if checkbounds([f_posx,f_posy],j1.rect,tiles) == False:
                        j1.vely = 5
                    else:
                        j1.rect.bottom += -15

            if  event.type == pygame.KEYUP: #para que se mueva solo cuando se pulse la tecla
                j1.velx = 0 
                j1.vely = 0 
                j1.entra2 = 1#comprobacion para el disparo


            #animacion disparo
            if  event.type == pygame.MOUSEBUTTONDOWN :
                balas = Bala()
                balas.rect.x = j1.rect.x + 20
                balas.rect.y = j1.rect.y + 20
                flechas.add(balas)
                if j1.entra2 == 1:#que dispare aunque el jugador se encuentre detenido
                    j1.entra = 1
                if event.button == 1:
                    if j1.fil == 11:
                        j1.fil =19
                    if j1.fil == 9:
                        j1.fil =17 
                    if j1.fil == 8:
                        j1.fil =16
                    if j1.fil == 10:
                        j1.fil =18  
            elif event.type == pygame.MOUSEBUTTONUP:
                j1.fil = anterior 
                if j1.entra2 == 1:#si el jugador esta detenido y no esta disparando que no realice ninguna accion
                    j1.entra = 0

                
        
        
        for element in flechas:
            #cuando se sale de la pantalla deja de existir
            if element.rect.y <= 0:#arriba
                flechas.remove(element)
            if element.rect.y >= 800:#abajo
                flechas.remove(element)
            if element.rect.x <= 0:#izquierda
                flechas.remove(element)
            if element.rect.x >= 800:
                flechas.remove(element)
            #cuando una flecha toca un generador, desaparece el generador y la flecha
            desaparece = pygame.sprite.spritecollide(element,gen,True)
            for index in desaparece:
                flechas.remove(element)
                j1.generadores += 1
            #cuando una flecha toca un zombie, desaparece el zombie y la flecha
            desaparece = pygame.sprite.spritecollide(element,zombies,True)
            for index in desaparece:
                flechas.remove(element)
                
            
                
        #cuando el jugador necesita vida puede usar un botiquin        
        desapareceBotiquin = pygame.sprite.spritecollide(j1,botiquines,True)
        for index in desapareceBotiquin:
            if j1.salud < 5:
                botiquines.remove(index)
                j1.salud += 1
            else: 
                botiquines.add(index)
                
                
        #cuando el jugador choca con un zombie
        mordida = pygame.sprite.spritecollide(j1,zombies,True)
        for index in mordida:
            j1.salud -= 1
            zombies.add(index)
            if j1.fil == 11:
                j1.rect.x -= 20
            if j1.fil == 9:
                j1.rect.x += 20
            if j1.fil == 8:
                j1.rect.y += 20
            if j1.fil == 10:
                j1.rect.y -= 20
                

         #desplazamiento mapa a la velocidad del jugador
        if (j1.rect.right >= 400) and (f_posx >= -1596):
            mov = j1.rect.right - 400
            j1.rect.right = 400
            f_posx += -mov

                    
        if (j1.rect.left <= 300) and (f_posx <= -10):
            mov =  300 - j1.rect.left
            j1.rect.left = 300
            f_posx += mov

            
        if (j1.rect.top <= 300) and (f_posy <= -10):
            mov =  300 - j1.rect.top
            j1.rect.top = 300
            f_posy += mov

            
        if (j1.rect.bottom >= 600) and (f_posy >= -1596):
            mov = j1.rect.bottom - 600
            j1.rect.bottom = 600
            f_posy += -mov
 
            
        
        informes1.informes = c1.abierto + c2.abierto + c3.abierto#cantidada de cofres abiertos = informes
        j1.informes = informes1.informes
        
 
        if (j1.informes == 3) and (j1.generadores == 4):#condiciones de victoria
            fin = True
            seleccion = 1
            sonido.stop()
            sonido = pygame.mixer.Sound("ganar.mp3")
            pygame.mixer.Sound.play(sonido, -1)
            Ganar = 1
        if (j1.salud <= 0):#condicion de derrota
            seleccion = 2
            fin = True
            sonido.stop()
            sonido = pygame.mixer.Sound("perder.wav")
            pygame.mixer.Sound.play(sonido, -1)
            Ganar = 0
            
           

  
        I.salud = j1.salud #la barra de vida va acorde a la salud del jugador  
        generadoresInformacion.generadores = j1.generadores#la informacion de los generadores cambia acorde a cuantos haya destruido el jugador     
            
        #actualizacion eventos
        jugadores.update()
        zombies.update([f_posx,f_posy])
        cofres.update([f_posx,f_posy],j1.rect) 
        botiquines.update([f_posx,f_posy],j1.rect) 
        Info.update()
        Informes1.update()
        flechas.update(j1.fil)
        gen.update([f_posx,f_posy],j1.rect)
        GeneradoresInfo.update()
        screen.blit(fondo[seleccion],(f_posx, f_posy))
        gen.draw(screen)
        flechas.draw(screen)
        cofres.draw(screen)
        botiquines.draw(screen)
        jugadores.draw(screen)
        zombies.draw(screen)
        Info.draw(screen)
        Informes1.draw(screen)
        GeneradoresInfo.draw(screen)
        pygame.display.flip()
        reloj.tick(15)


    jugadores.update()
    zombies.update([f_posx,f_posy])
    cofres.update([f_posx,f_posy],j1.rect) 
    botiquines.update([f_posx,f_posy],j1.rect) 
    Info.update()
    Informes1.update()
    flechas.update(j1.fil)
    gen.update([f_posx,f_posy],j1.rect)
    GeneradoresInfo.update()
    screen.blit(fondo[0],(f_posx, f_posy))
    gen.draw(screen)
    flechas.draw(screen)
    cofres.draw(screen)
    botiquines.draw(screen)
    jugadores.draw(screen)
    zombies.draw(screen)
    Info.draw(screen)
    Informes1.draw(screen)
    GeneradoresInfo.draw(screen)
    pygame.display.flip()
    pygame.time.wait(2000) 
    screen.fill(negro)
    screen.blit(fondo[seleccion],(0,0))
    pygame.display.flip()
    pygame.time.wait(5000) #espera 2 segundos antes de cerrar la ventana
    sonido.stop()
    return Ganar
    
    
