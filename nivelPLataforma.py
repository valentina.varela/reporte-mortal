import pygame
import pytmx
import math
import threading
import time

#Background color
BACKGROUND = (20, 20, 20)
negro = [0,0,0]
rojo = [255,0,0]
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 800
pygame.init()
FUENTE = pygame.font.Font("stocky.ttf", 30)


#Tiled map layer of tiles that you collide with
MAP_COLLISION_LAYER = 1

class Game(object):
    def __init__(self,screen,jugador,cofres,infoCofres,avisos,infoAvisos,salud,botiquines,estatico,dinamico,jefe):
        #Set up a level to load
        self.screen = screen
        self.m = jugador
        self.currentLevelNumber = 0
        self.levels = []
        self.levels.append(Level(fileName ="plataforma.tmx"))
        self.currentLevel = self.levels[self.currentLevelNumber]
        self.vidas = 20
        self.texto = pygame.Surface((40,20))
        self.ganar = 3
        self.comprobacion = ""
             
        #eventos
        self.avisos = avisos
        self.infoAvisos = infoAvisos     
        
        #Create a player object and set the level it is in
        self.player = Player(x = 10, y = 50,m = jugador)
        self.player.currentLevel = self.currentLevel
        
        
        #cofres
        self.cofres = cofres
        self.infoCofres = infoCofres
        
        #vida
        self.salud = salud
        self.botiquines = botiquines
        
        
        #enemigos : estatico
        self.estatico = estatico
        
        #enemigos : dinamico
        self.dinamico = dinamico
        
        #Jefe
        self.jefe = jefe
        
        #Draw aesthetic overlay
        self.overlay = pygame.image.load("overlay.png")
        

        
    def processEvents(self):
        
        for event in pygame.event.get():
            self.player.disparar = 0
            if event.type == pygame.QUIT:
                return True
            #Get keyboard input and move player accordingly
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_a:
                    self.player.goLeft()
                elif event.key == pygame.K_d:
                    self.player.goRight()
                elif event.key == pygame.K_w:
                    self.player.jump()
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_a and self.player.changeX < 0:
                    self.player.stop()
                elif event.key == pygame.K_d and self.player.changeX > 0:
                    self.player.stop()
            if event.type == pygame.MOUSEBUTTONDOWN:
                self.player.shot()
    
        return False
        
    def runLogic(self):
        
        #Update player movement and collision logic
        self.texto,self.comprobacion = self.player.update(self.screen,self.cofres,self.avisos,self.botiquines,self.estatico,self.dinamico,self.jefe,self.vidas,self.texto)
        self.player.balas.update()
        self.infoCofres.update(self.cofres)
        self.infoAvisos.update(self.avisos,self.cofres)
        self.salud.update(self.player.vida)

 
        
    
    #Draw level, player, overlay
    def draw(self, screen):
        screen.fill(BACKGROUND)
        self.currentLevel.draw(screen)
        self.cofres.draw(screen)
        self.botiquines.draw(screen)
        self.player.draw(screen)
        self.player.balas.draw(screen)
        self.jefe.draw(screen)
        for element in self.jefe:
            element.bullets.draw(screen)
        self.estatico.draw(screen)
        self.dinamico.draw(screen)
        for element in self.estatico:
            element.bullets.draw(screen)
        screen.blit(self.overlay, [0, 0])
        self.infoCofres.draw(screen)
        self.avisos.draw(screen)
        screen.blit(self.texto, (550,650))
        self.infoAvisos.draw(screen)
        self.salud.draw(screen)
        pygame.display.flip()
        if (self.comprobacion == "Ganador")or (self.comprobacion =="Game over") :
            pygame.time.wait(3000)
            pygame.quit()

        



class Bullet(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.pos = (x, y)
        mx, my = pygame.mouse.get_pos()
        self.dir = (mx - x, my - y)
        length = math.hypot(*self.dir)
        if length == 0.0:
            self.dir = (0, -1)
        else:
            self.dir = (self.dir[0]/length, self.dir[1]/length)
        angle = math.degrees(math.atan2(-self.dir[1], self.dir[0]))


        self.image = pygame.image.load("disparo.png")
        self.rect = self.image.get_rect(center = self.pos)
        self.image = pygame.transform.rotate(self.image, angle)
        self.speed = 13

    def update(self):  
        self.pos = (self.pos[0]+self.dir[0]*self.speed, 
                    self.pos[1]+self.dir[1]*self.speed)
        
        self.rect.x = self.pos[0]+self.dir[0]*self.speed
        self.rect.y = self.pos[1]+self.dir[1]*self.speed
        
        if self.pos[0] >= 800 or self.pos[0] <= 0:
            self.kill()
        if self.pos[1] >= 800 or self.pos[1] <= 0:
            self.kill()



    
        
class Player(pygame.sprite.Sprite):
    def __init__(self, x, y,m):
        pygame.sprite.Sprite.__init__(self)
        self.m = m
        self.col = 0
        self.fil = 10
        self.image = self.m[self.fil][self.col]
        self.vida = 10
        self.disparar = 0
        self.balas = pygame.sprite.Group()
        self.cantidad = 0
        
        self.stillRight = self.m[11][0]
        self.stillLeft = self.m[9][0]
        
        #List of frames for each animation
        self.runningRight = (self.m[11][0],self.m[11][1],self.m[11][2],self.m[11][3],self.m[11][4],self.m[11][5],self.m[11][6],self.m[11][7])

        self.runningLeft = (self.m[9][0],self.m[9][1],self.m[9][2],self.m[9][3],self.m[9][4],self.m[9][5],self.m[9][6],self.m[9][7])
                    
        self.jumpingRight = (self.m[3][0],self.m[3][1],self.m[3][2],self.m[3][3],self.m[3][4],self.m[3][5],self.m[3][6],self.m[3][7])

        self.jumpingLeft = (self.m[1][0],self.m[1][2],self.m[1][3],self.m[1][4],self.m[1][5],self.m[1][6],self.m[1][7])
        
        self.shotRight = (self.m[19][4],self.m[19][5],self.m[19][6],self.m[19][7],self.m[19][8],self.m[19][9],self.m[19][10],self.m[19][11])
        self.shotLeft = (self.m[17][4],self.m[17][5],self.m[17][6],self.m[17][7],self.m[17][8],self.m[17][9],self.m[17][10],self.m[17][11])
        
        #Set player position
        self.rect = pygame.Rect(0,0,42,60)
        self.rect.x = x
        self.rect.y = y
        
        #Set speed and direction
        self.changeX = 0
        self.changeY = 0
        self.direction = "right"
        
        #Boolean to check if player is running, current running frame, and time since last frame change
        self.running = False
        self.runningFrame = 0
        self.runningTime = pygame.time.get_ticks()
        
        #Players current level, set after object initialized in game constructor
        self.currentLevel = None
        
    def update(self,screen,cofres,avisos,botiquines,estatico,dinamico,jefe,vidas,texto):
        #Update player position by change
        self.rect.x += self.changeX
        
        #Get tiles in collision layer that player is now touching
        tileHitList = pygame.sprite.spritecollide(self, self.currentLevel.layers[MAP_COLLISION_LAYER].tiles, False)
        
        #Move player to correct side of that block
        for tile in tileHitList:
            if self.changeX > 0:
                self.rect.right = tile.rect.left
            else:
                self.rect.left = tile.rect.right

        avisos.update(self.rect,0,0)
        botiquines.update(self.rect,0,0)
        estatico.update(self.rect,0,0)
        dinamico.update(self.rect,0,0)
        jefe.update(self.rect,0,0)
        for element in jefe:
            element.bullets.update(0,0)
        for element in estatico:
            element.bullets.update(0,0)
        #Move screen if player reaches screen bounds
        if self.rect.right >= SCREEN_WIDTH - 200:
            difference = self.rect.right - (SCREEN_WIDTH - 200)
            self.rect.right = SCREEN_WIDTH - 200
            self.currentLevel.shiftLevel(-difference,0)
            cofres.update(self.rect,-difference,0)
            avisos.update(self.rect,-difference,0)
            botiquines.update(self.rect,-difference,0)
            estatico.update(self.rect,-difference,0)
            dinamico.update(self.rect,-difference,0)
            jefe.update(self.rect,-difference,0)
            for element in jefe:
                element.bullets.update(-difference,0)
            for element in estatico:
                element.bullets.update(-difference,0)
        
        #Move screen is player reaches screen bounds
        if self.rect.left <= 200:
            difference = 200 - self.rect.left
            self.rect.left = 200
            self.currentLevel.shiftLevel(difference,0)
            cofres.update(self.rect,difference,0)
            avisos.update(self.rect,difference,0)
            botiquines.update(self.rect,difference,0)
            estatico.update(self.rect,difference,0)
            dinamico.update(self.rect,difference,0)
            jefe.update(self.rect,difference,0)
            for element in jefe:
                element.bullets.update(difference,0)
            for element in estatico:
                element.bullets.update(difference,0)
            
        #Move screen if player reaches screen bounds
        if self.rect.top <= 200:
            difference = 200 - self.rect.top
            self.rect.top = 200
            self.currentLevel.shiftLevel(0,difference)
            cofres.update(self.rect,0,difference)
            avisos.update(self.rect,0,difference)
            botiquines.update(self.rect,0,difference)
            estatico.update(self.rect,0,difference)
            dinamico.update(self.rect,0,difference)
            jefe.update(self.rect,0,difference)
            for element in jefe:
                element.bullets.update(0,difference)
            for element in estatico:
                element.bullets.update(0,difference)
            
        #Move screen is player reaches screen bounds
        if self.rect.bottom >= SCREEN_HEIGHT - 200:
            difference = self.rect.bottom - (SCREEN_HEIGHT - 200)
            self.rect.bottom= SCREEN_HEIGHT - 200
            self.currentLevel.shiftLevel(0,-difference)
            cofres.update(self.rect,0,-difference)
            avisos.update(self.rect,0,-difference)
            botiquines.update(self.rect,0,-difference)
            estatico.update(self.rect,0,-difference)
            dinamico.update(self.rect,0,-difference)
            jefe.update(self.rect,0,-difference)
            for element in jefe:
                element.bullets.update(0,-difference)
            for element in estatico:
                element.bullets.update(0,-difference)
        
        #Update player position by change
        self.rect.y += self.changeY
        
        #Get tiles in collision layer that player is now touching
        tileHitList = pygame.sprite.spritecollide(self, self.currentLevel.layers[MAP_COLLISION_LAYER].tiles, False)
       
        #If there are tiles in that list
        if len(tileHitList) > 0:
            #Move player to correct side of that tile, update player frame
            for tile in tileHitList:
                if self.changeY > 0:
                    self.rect.bottom = tile.rect.top
                    self.changeY = 1
                    
                    if self.direction == "right":
                        self.image = self.stillRight
                    else:
                        self.image = self.stillLeft
                else:
                    self.rect.top = tile.rect.bottom
                    self.changeY = 0
        #If there are not tiles in that list
        else:
            #Update player change for jumping/falling and player frame
            self.changeY += 0.2
            if self.changeY > 0:
                if self.direction == "right":
                    self.image = self.jumpingRight[1]
                else:
                    self.image = self.jumpingLeft[1]
        
        #If player is on ground and running, update running animation
        if self.running and self.changeY == 1:
            if self.direction == "right":
                self.image = self.runningRight[self.runningFrame]
            else:
                self.image = self.runningLeft[self.runningFrame]
        
        aux = self.image      

            
        if self.disparar == 1:
            if self.direction == "right":
                self.image = self.shotRight[self.runningFrame]
            if self.direction == "left":
                self.image = self.shotLeft[self.runningFrame]
        else:
            self.image = aux
            
        
        #When correct amount of time has passed, go to next frame
        if pygame.time.get_ticks() - self.runningTime > 50:
            self.runningTime = pygame.time.get_ticks()
            if self.runningFrame == 7:
                self.runningFrame = 0
            else:
                self.runningFrame += 1
        
  
        desapareceBotiquin = pygame.sprite.spritecollide(self,botiquines,True)
        for index in desapareceBotiquin:
            if self.vida < 10:
                botiquines.remove(index)
                self.vida += 1
            else:
                botiquines.add(index)
                
        for element in estatico: #cuando una bola de acido toca al jugador desaparece y le baja vida    
            desapareceBalas = pygame.sprite.spritecollide(self,element.bullets,True)
            for index in desapareceBalas:
                element.bullets.remove(index)
                self.vida = self.vida - 1
                
                
        for element in estatico:#cuando una flecha toca un zombie desaparece
            desapareceZombie = pygame.sprite.spritecollide(element,self.balas,True)
            for index in desapareceZombie:
                element.kill()
                

        self.cantidad += 1
        if self.cantidad >= 30: #para que la closion no mate tan rapido al jugador
            self.cantidad = 1
        desapareceZombieD = pygame.sprite.spritecollide(self,dinamico,True)
        for index in desapareceZombieD:
            if self.cantidad == 1:
                self.vida -= 1
            dinamico.add(index)
                
        for element in dinamico:
            desapareceBalasD = pygame.sprite.spritecollide(element,self.balas,True)
            for index in desapareceBalasD:
                element.kill()
        
    
        for element in jefe: #cuando una bola de acido toca al jugador desaparece y le baja vida    
            desapareceBalas = pygame.sprite.spritecollide(self,element.bullets,True)
            for index in desapareceBalas:
                element.bullets.remove(index)
                self.vida = self.vida - 1
        
        
        for element in jefe:#cuando una flecha toca un jefe le baja vida
            desapareceZombie = pygame.sprite.spritecollide(element,self.balas,True)
            for index in desapareceZombie:
                element.vida -= 1
            if element.vida <= 0:
                element.kill()
        
        vidas = 0
        for element in jefe:
            vidas += element.vida      
        
         
        ene = "Vidas Jefe: "+str(vidas)       
        texto = FUENTE.render(ene, 0, rojo)
     
        a = ""
        if vidas == 0:
            a = "Ganador"
            texto = FUENTE.render("Ganador", 0, rojo)
                        
        if self.vida == 0:
            #pygame.quit()
            a = "Game over"
            texto = FUENTE.render("Game over", 0, rojo)
            
        return texto,a

                
    
    #Make player jump
    def jump(self):
        #Check if player is on ground
        self.rect.y += 2
        tileHitList = pygame.sprite.spritecollide(self, self.currentLevel.layers[MAP_COLLISION_LAYER].tiles, False)
        self.rect.y -= 2
        
        if len(tileHitList) > 0:
            if self.direction == "right":
                self.image = self.jumpingRight[0]
            else:
                self.image = self.jumpingLeft[0]
                
            self.changeY = -6
    
    #Move right
    def goRight(self):
        self.direction = "right"
        self.running = True
        self.changeX = 3
    
    #Move left
    def goLeft(self):
        self.direction = "left"
        self.running = True
        self.changeX = -3
        
    def shot(self):
        self.disparar = 1
        sonido = pygame.mixer.Sound("sonidos/arrow.mp3")
        pygame.mixer.Sound.play(sonido, 1)
        sonido.fadeout(1000)
        bullet = Bullet(self.rect.centerx, self.rect.top)
        self.balas.add(bullet)
    
    #Stop moving
    def stop(self):
        self.running = False
        self.changeX = 0
    
    #Draw player
    def draw(self, screen):
        screen.blit(self.image, self.rect)
        




class Jefe(pygame.sprite.Sprite):
    def __init__(self,m,x,y,evento):
        pygame.sprite.Sprite.__init__(self)
        self.m = m
        self.col = 4
        self.fil = 2
        self.image = self.m[self.fil][self.col]
        self.aux = self.image
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.evento = evento
        self.bullets = pygame.sprite.Group()
        self.cantidad = 0
        self.vida = 10
        
    def shot(self):
        bullet = BulletEnemigo(self.fil)

        #Establecer la ubicación del asistente
        bullet.rect.bottom = self.rect.y + 35
        bullet.rect.centerx = self.rect.centerx

        #Agregar el asistente al grupo de asistentes
        self.bullets.add(bullet)
        
        
        bullet2 = BulletEnemigo(self.fil)

        #Establecer la ubicación del asistente
        bullet2.rect.bottom = self.rect.y + 35
        bullet2.rect.centerx = self.rect.centerx + 30

        #Agregar el asistente al grupo de asistentes
        self.bullets.add(bullet2)
        
        bullet3 = BulletEnemigo(self.fil)

        #Establecer la ubicación del asistente
        bullet3.rect.bottom = self.rect.y + 35
        bullet3.rect.centerx = self.rect.centerx - 30

        #Agregar el asistente al grupo de asistentes
        self.bullets.add(bullet3)
    
    def update(self,jugador,movX,movY):
        self.rect.x += movX
        self.rect.y += movY
        self.cantidad += 1   
        self.image = pygame.image.load("img/transparente.png")
        if self.evento == 0:
            if self.cantidad <= 200:
                self.image = self.aux
            if self.cantidad == 50 or self.cantidad == 100 or self.cantidad == 150:
                self.shot()
            if self.cantidad == 500:
                self.cantidad = 0
        if self.evento == 1:
            if self.cantidad <= 400 and self.cantidad > 200:
                self.image = self.aux
            if self.cantidad == 250 or self.cantidad == 300 or self.cantidad == 350:
                self.shot()
            if self.cantidad == 500:
                self.cantidad = 0
        
            
            
 

        
                
            
            


class Estatico(pygame.sprite.Sprite):
    def __init__(self,m,x,y,direccion):
        pygame.sprite.Sprite.__init__(self)
        self.m = m
        self.col = 0
        self.fil = direccion
        self.image = self.m[self.fil][self.col]
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.bullets = pygame.sprite.Group()
        self.cantidad = 0
     
    
    def shot(self):
        bullet = BulletEnemigo(self.fil)

        #Establecer la ubicación del asistente
        bullet.rect.bottom = self.rect.y + 35
        bullet.rect.centerx = self.rect.centerx

        #Agregar el asistente al grupo de asistentes
        self.bullets.add(bullet)
    
    def update(self,jugador,movX,movY):
        self.rect.x += movX
        self.rect.y += movY
        self.cantidad += 1   
        if self.cantidad == 20:
            self.shot()
            self.cantidad = 0
            
  



class BulletEnemigo(pygame.sprite.Sprite):
    def __init__(self,direccion):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("bola.png")
        self.rect = self.image.get_rect()
        self.vel = 13
        self.direccion = direccion
        self.cantidad = 0
        
        
    def update(self,movX,movY):
        self.rect.x += movX
        self.rect.y += movY
        self.cantidad +=1
        
        if self.direccion == 3:
            self.rect.x += self.vel
            if self.cantidad >= 20:#distancia de disparo
                self.kill()
                self.cantidad = 0
        if self.direccion == 1:
            self.rect.x -= self.vel
            if self.cantidad >= 20:#distancia de disparo
                self.kill()
                self.cantidad = 0
        if self.direccion == 2:
            self.rect.y += self.vel
            if self.cantidad >= 30:#distancia de disparo
                self.kill()
                self.cantidad = 0
                

          
            
class Dinamico(pygame.sprite.Sprite):
    def __init__(self,m,x,y,lim1,lim2,lim3,lim4):
        pygame.sprite.Sprite.__init__(self)
        self.m = m
        self.col = 0
        self.fil = 9
        self.image = self.m[self.fil][self.col]
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.izquierda = lim1
        self.derecha = lim2
        self.arriba = lim3
        self.abajo = lim4
        self.velx = 1
        
    def update(self,jugador,movX,movY):
        self.rect.x += movX
        self.rect.y += movY
        self.izquierda += movX
        self.derecha += movX
        self.arriba += movY
        self.abajo += movY
        self.rect.x += self.velx
        
        self.image = self.m[self.fil][self.col]
        
        if (self.fil <= 3) and (self.fil >=0):
            if self.col < 6:
                self.col += 1
            else:
                self.col = 0
        if (self.fil <= 7) and (self.fil >=4):
            if self.col < 7:
                self.col += 1
            else:
                self.col = 0
        if (self.fil <= 11) and (self.fil >=8):
            if self.col < 8:
                self.col += 1
            else:
                self.col = 0
        if ((self.fil <= 15) and (self.fil >=12)) or self.fil == 20:
            if self.col < 5:
                self.col += 1
            else:
                self.col = 0
        if (self.fil <= 19) and (self.fil >=16):
            if self.col < 12:
                self.col += 1
            else:
                self.col = 0
                
        if self.rect.x < self.izquierda:
            self.velx = 1
            self.fil = 11

        # Limita el margen derecho
        if self.rect.x > self.derecha:
            self.velx = -1
            self.fil = 9
            
        if self.rect.x >= self.izquierda and self.rect.x <= self.derecha:
            if jugador.x >= self.izquierda and jugador.x <= self.derecha:
                if jugador.y >= self.arriba and jugador.y <= self.abajo :
                    if jugador.x > self.rect.x:
                        self.velx = 1
                        self.fil = 11
                    else:
                        self.velx = -1
                        self.fil = 9
            
        

            
        

            


class Eventos(pygame.sprite.Sprite):
    def __init__(self,m,x,y):
        pygame.sprite.Sprite.__init__(self)
        self.image = m
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.evento = 0
        self.pasa = 0
        self.repetir = 0
        
       
    def update(self,jugador,movX,movY):
        self.rect.x += movX
        self.rect.y += movY
        if (jugador.colliderect(self.rect)): 
            self.pasa = 1
        else:
            self.pasa = 0

class Info(pygame.sprite.Sprite):
    def __init__(self,m):
        pygame.sprite.Sprite.__init__(self)
        self.image = m
        self.aux = m
        self.rect = self.image.get_rect()
        self.rect.x = 20
        self.rect.y = 560
        
    def update(self,lista,informes):
        self.image = self.aux
        cantidad = 0
        for element in informes:
            cantidad += element.abierto
        
        for element in lista:
            if element.pasa == 1:
                if element.evento == 0:
                    self.image = pygame.image.load("img/dialogo_inicio.png")
                if element.evento == 1:
                    if cantidad < 3:
                        self.image = pygame.image.load("img/dialogo_paso.png")
                if element.evento == 2:
                    if element.repetir <= 70:
                        self.image = pygame.image.load("img/dialogo_jefe.png")
                        element.repetir += 1
                        
                        
                    


class Cofres(pygame.sprite.Sprite):
    def __init__(self,m,posicion):
        pygame.sprite.Sprite.__init__(self)
        self.m = m
        self.col = 3
        self.fil = 0 #animacion de abrir y cerrar cofre
        self.image = self.m[self.fil][self.col]
        self.rect = self.image.get_rect()
        self.rect.x = posicion[0]
        self.rect.y = posicion[1]
        self.abierto = 0
       
    def update(self,jugador,movX,movY):
        self.image = self.m[self.fil][self.col]
        self.rect.x += movX
        self.rect.y += movY
        if (jugador.colliderect(self.rect)): 
            self.abierto = 1
            self.fil = 3


class Informes(pygame.sprite.Sprite):
    def __init__(self,m):
        pygame.sprite.Sprite.__init__(self)
        self.m = m
        self.col = 0
        self.fil = 0 #esta es la que cambia la cantidad de informes
        self.image = self.m[self.fil][self.col]
        self.rect = self.image.get_rect()
        self.rect.x = 530
        self.rect.y = 680
        
    def update(self,lista):
        self.image = self.m[self.fil][self.col]
        cantidad = 0
        for element in lista:
            cantidad += element.abierto
            
        if cantidad == 0:
            self.fil = 0
        if cantidad == 1:
            self.fil = 1
        if cantidad == 2:
            self.fil = 2
        if cantidad == 3:
            self.fil = 3



class Salud(pygame.sprite.Sprite):
    def __init__(self,m):
        pygame.sprite.Sprite.__init__(self)
        self.m = m
        self.col = 0
        self.fil = 0 #esta es la que cambia la imagen de la vida
        self.image = self.m[self.fil][self.col]
        self.rect = self.image.get_rect()
        self.rect.x = 10
        self.rect.y = 10
        self.salud = 10
        
    def update(self,vidaJugador):
        self.salud = vidaJugador
        self.image = self.m[self.fil][self.col]
        if self.salud == 10:
            self.fil = 0
        if self.salud == 8 or self.salud == 9:
            self.fil = 1
        if self.salud == 6 or self.salud == 7:
            self.fil = 2
        if self.salud == 4 or self.salud == 5:
            self.fil = 3
        if self.salud == 2 or self.salud == 3 or self.salud == 1 or self.salud == 0:
            self.fil = 4



class Botiquines(pygame.sprite.Sprite):
    def __init__(self,m,x,y):
        pygame.sprite.Sprite.__init__(self)
        self.m = m
        self.col = 0
        self.fil = 0 #animacion de abrir y cerrar cofre
        self.image = self.m[self.fil][self.col]
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
       
    def update(self,jugador,movX,movY):
        self.rect.x += movX
        self.rect.y += movY




#Mapa        
class Level(object):
    def __init__(self, fileName):
        #Create map object from PyTMX
        self.mapObject = pytmx.load_pygame(fileName)
        
        #Create list of layers for map
        self.layers = []
        
        #Amount of level shift left/right
        self.levelShift = 0
        #top/bottom
        self.levelV = 0
        
        #Create layers for each layer in tile map
        for layer in range(len(self.mapObject.layers)):
            self.layers.append(Layer(index = layer, mapObject = self.mapObject))
    
    #Move layer left/right
    def shiftLevel(self, shiftX,shiftY):
        self.levelShift += shiftX
        self.levelV += shiftY
        
        for layer in self.layers:
            for tile in layer.tiles:
                tile.rect.x += shiftX
                tile.rect.y += shiftY
    
    #Update layer
    def draw(self, screen):
        for layer in self.layers:
            layer.draw(screen)
            
class Layer(object):
    def __init__(self, index, mapObject):
        #Layer index from tiled map
        self.index = index
        
        #Create gruop of tiles for this layer
        self.tiles = pygame.sprite.Group()
        
        #Reference map object
        self.mapObject = mapObject
        
        #Create tiles in the right position for each layer
        for x in range(self.mapObject.width):
            for y in range(self.mapObject.height):
                img = self.mapObject.get_tile_image(x, y, self.index)
                if img:
                    self.tiles.add(Tile(image = img, x = (x * self.mapObject.tilewidth), y = (y * self.mapObject.tileheight)))

    #Draw layer
    def draw(self, screen):
        self.tiles.draw(screen)

#Tile class with an image, x and y
class Tile(pygame.sprite.Sprite):
    def __init__(self, image, x, y):
        pygame.sprite.Sprite.__init__(self)
        
        self.image = image
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

      
      
def recortar(i_ancho,i_alto,m):#funcion de recorte de sprites
    info=m.get_rect()
    f_ancho=info[2]
    f_alto=info[3]
    mls=list()
    ls=list()
    filas=i_alto
    col=i_ancho
    corte_ancho=int(f_ancho/col)
    corte_alto=int(f_alto/filas)
    for i in range(filas):
        ls=[]
        for j in range(col):
            cuadrado =m.subsurface((corte_ancho*j),(corte_alto*i),corte_ancho,corte_alto)
            ls.append(cuadrado)
        mls.append(ls)

    return mls


  
def nivel2(screen):

    clock = pygame.time.Clock()
    done = False


    sonido = pygame.mixer.Sound("historia.wav")
    pygame.mixer.Sound.play(sonido, 1)
    inicio = pygame.image.load("img/nivel2.png")
    screen.fill(negro)
    screen.blit(inicio,(0,0))
    pygame.display.flip()
    pygame.time.wait(15000)
    sonido.fadeout(2000)
  
    
    #JUGADOR
    img=pygame.image.load('arco.png')
    jugador = recortar(13, 21, img) 
    

    #Avisos
    avisos=pygame.sprite.Group()
    img=pygame.image.load('img/transparente.png')
    img = pygame.transform.scale(img, (200, 100))
    c1 = Eventos(img,48,96)
    c1.evento = 0
    avisos.add(c1)
    c2 = Eventos(img,2011,676) 
    c2.evento = 1
    avisos.add(c2)
    img = pygame.transform.scale(img, (50, 100))
    c3 = Eventos(img,2781,687) 
    c3.evento = 2
    avisos.add(c3)


    InfoAvisos =   pygame.sprite.Group()  
    imagen=pygame.image.load('img/transparente.png')
    informes1 = Info(imagen)
    InfoAvisos.add(informes1)
    
    
    
    #COFRES
    cofresInformes=pygame.sprite.Group()
    img=pygame.image.load('cofres.png')
    m = recortar(12, 8, img) #[fila][columna]
    c1 = Cofres(m,[1097,41])
    cofresInformes.add(c1)
    c2 = Cofres(m,[1391,439])
    cofresInformes.add(c2)
    c3 = Cofres(m,[1702,634])
    cofresInformes.add(c3)

    
    #Informacion cofres
    Informes1 =   pygame.sprite.Group()  
    imagen=pygame.image.load('informes.png')
    imagen_redimensionada = pygame.transform.scale(imagen, (250, 250))
    m = recortar(1, 4, imagen_redimensionada) #[fila][columna]   
    informes1 = Informes(m)
    Informes1.add(informes1)
    
    
    #Barra vida
    salud =   pygame.sprite.Group()  
    imagen=pygame.image.load('vida.png')
    imagen_redimensionada = pygame.transform.scale(imagen, (230, 230))
    m = recortar(1, 5, imagen_redimensionada) #[fila][columna] 
    I = Salud(m)
    salud.add(I)
    
    #Botiquines
    botiquines=pygame.sprite.Group()
    img=pygame.image.load('botiquin.png')
    img = pygame.transform.scale(img, (40, 40))
    m = recortar(1, 1, img) #[fila][columna]
    b1 = Botiquines(m,437,189)
    botiquines.add(b1)
    b1 = Botiquines(m,818,653)
    botiquines.add(b1)
    b1 = Botiquines(m,2143,731)
    botiquines.add(b1)
    
    #Enemigos estaticos
    lista = [(50,349,3),(263,572,1),(586,334,1),(2032,348,1),(830,652,3),(1490,622,1),(2663,475,1),(2536,412,3),(938,107,1),(1351,652,1),(2536,557,3)]
    enemigoEstatico = pygame.sprite.Group()
    img=pygame.image.load('zombie.png')
    m = recortar(13, 21, img)
    for element in lista:
        z = Estatico(m,element[0],element[1],element[2])
        enemigoEstatico.add(z)
        
    #Enemigos dinamicos
    l = [(271,725,31,455,703,786),(82,725,31,455,703,786),(430,725,31,455,703,786),
         (870,179,821,1837,183,241),(1074,179,821,1837,183,241),(1275,179,821,1837,183,241),(1735,179,821,1837,183,241),
         (1155,418,976,1868,400,488),(1479,418,976,1868,400,488),(1872,418,976,1868,400,488),(1057,721,895,1198,721,789),
         (2034,721,1859,2490,721,789),(2222,721,1859,2490,721,789),(2426,721,1859,2490,721,789)]
    dinamico = pygame.sprite.Group()
    img=pygame.image.load('jefe.png')
    m = recortar(13, 21, img)
    for element in l:
        z = Dinamico(m,element[0],element[1],element[2],element[3],element[4],element[5])
        dinamico.add(z)
        
    #Jefe
    jefe = pygame.sprite.Group()
    imagen = pygame.image.load('laura.png')
    imagen = pygame.transform.scale(imagen, (2000, 2000))
    imagen = recortar(13, 21,imagen)
    z = Jefe(imagen,3000,401,0)
    jefe.add(z)
    z1 = Jefe(imagen,2848,401,1)
    jefe.add(z1)

    
    game = Game(screen,jugador,cofresInformes,Informes1,avisos,InfoAvisos,salud,botiquines,enemigoEstatico,dinamico,jefe)
    
    sonido.stop()
    sonido = pygame.mixer.Sound("ciclo.mp3")
    pygame.mixer.Sound.play(sonido, -1)
    while not done:
        clock.tick(60)
        done= game.processEvents()
        game.runLogic()#update
        game.draw(screen)
  
    return True