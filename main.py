import pygame 
import nivelRPG
import nivelPLataforma


verde = [0,255,0]
rojo = [255,0,0]
azul = [0,0,255]
blanco = [255,255,255]
negro = [0,0,0]
turquesa = [83,166,154]
lila = [203,139,218]
naranja = [225,170,159]


ancho = 800
alto = 800


def portada(screen):
    sonido = pygame.mixer.Sound("historia.wav")
    pygame.mixer.Sound.play(sonido, 1)
    inicio = pygame.image.load("img/portada.png")
    screen.fill(negro)
    screen.blit(inicio,(0,0))
    pygame.display.flip()
    pygame.time.wait(3000)
    sonido.fadeout(2000)



if __name__ == "__main__":
    pygame.init()
    screen = pygame.display.set_mode([ancho, alto])
    pygame.display.set_caption("Reporte mortal")
    
    fin = False
    
    while not fin:
        
        #portada
        portada(screen)
        
        """
        fin1 = 0
        while fin1 == 0:
            fin1 = nivelRPG.nivel1(screen)
            
        fin2 = 0
        while fin2 == 0:
            fin2 = nivelPlataforma.nivel2(screen)
            
        fin = True

        """
        fin = nivelPLataforma.nivel2(screen)

        
        
    pygame.quit()
